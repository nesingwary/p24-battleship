﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace battleship
{
    internal class Program
    {
        public enum Mode  //Permet de select un mode d'affichage du programme, selon nos confitions IF pre-etabies.
        {
            Test,
            Play
        }

        //LES CASES OCCUPERONT EXACTEMENT 9 DE LARGE PAR 5 DE HAUT
        public static string caseJeu = @"
    _______
   |       |   
   |       |         
   |_______|        
            ";

        public static char lvert = '|';             //1 barre verticale
        public static string lhorz = "_______";     //9 lignes     
        public static string espace = "       ";    //9 espaces
        public static char ss = ' ';                //single space

        public static string doItAgain;
        public static int useri, userj;
        public static string struseri, struserj;
        public static bool isTrue;
        public static ushort sCount = 0;
        public static ushort gameCount = 0;

        static void Main(string[] args)
        {
            //MÉTHOE FANCYSTRING
            void TypeLine(string line)
            {
                for (int k = 0; k < line.Length; k++)
                {
                    Console.Write(line[k]);
                    System.Threading.Thread.Sleep(8);              //Sleep for 10 milliseconds
                }
            }

            //MODE DE JEU (BATEAU INVISIBLE = Play | BATEAU VISIBLE = Test)
            Mode mode = Mode.Test; //nos conditions IF comportant le mode TEST seront executees

            do //Boucle initiale du jeu
            {
                //Introduction animee
                Clear();
                Console.ForegroundColor = ConsoleColor.White;
                TypeLine("Bienvenue au jeu BATTLESHIP!\n");
                System.Threading.Thread.Sleep(1100);
                Console.ForegroundColor = ConsoleColor.Green;
                TypeLine("Un bateau se trouve sur la carte qui apparaitra ci-dessous.\nPar contre, il sera à vous de trouver sa position par essai erreur.\nLe bateau a une dimension de 1 x 3, et sa position et son orientation est totalement aléatoire.\nBonne chance!\n");
                WriteLine("Pesez sur ENTER pour commencer!");
                ReadLine();
                Clear();
                System.Threading.Thread.Sleep(1000);

                //variables de depart
                char[,] points = new char[10, 10];
                int i; //lignes
                int j; //colonnes
                int boatLength = 3;
                sCount = 0;

                //*********************************************************************************1-Remplissage du tableau par des points 
                for (i = 0; i < points.GetLength(0); i++)                    //iteration des LIGNES
                {
                    for (j = 0; j < points.GetLength(1); j++)                //iteration des COLONNES
                    {
                        points[i, j] = '.';                
                    }
                }

                //********************************************************************************* 2-assignation du centre du sous-marin dans le tableau points 
                Random rand = new Random();
                int xrand = rand.Next(1, points.GetLength(0));                      //centre du bateau en x    
                int yrand = rand.Next(1, points.GetLength(1));                      //centre du bateau en y
                points[xrand, yrand] = 'S';

                //********************************************************************************* 3-randomisation de l'emplacement et de l'orientation du bateau 
                Random orientation = new Random();
                int vert;
                vert = 1;

                if (orientation.Next(2) == vert) //CONDITION 1-1: le bateau peut se permettre d'etre vertical          
                {
                    if (xrand < (points.GetLength(0) - (points.GetLength(0) % boatLength)) && xrand > (points.GetLength(0) % boatLength))
                    {
                        points[xrand + (points.GetLength(0) % boatLength), yrand] = 'S';
                        points[xrand - (points.GetLength(0) % boatLength), yrand] = 'S';
                    }
                    else //CONDITION 1-2: le bateau n'a pas le choix d'etre horizontal
                    {
                        points[xrand, yrand + (points.GetLength(0) % boatLength)] = 'S';
                        points[xrand, yrand - (points.GetLength(0) % boatLength)] = 'S';
                    }

                }
                else //CONDITION 2-1: le bateau peut se permettre d'etre horizontal                                          
                {
                    if (yrand < (points.GetLength(0) - (points.GetLength(0) % boatLength)) && yrand > (points.GetLength(0) % boatLength))
                    {
                        points[xrand, yrand + (points.GetLength(0) % boatLength)] = 'S';
                        points[xrand, yrand - (points.GetLength(0) % boatLength)] = 'S';
                    }
                    else //CONDITION 2-2: le bateau n'a pas le choix d'etre vertical
                    {
                        points[xrand + (points.GetLength(0) % boatLength), yrand] = 'S';
                        points[xrand - (points.GetLength(0) % boatLength), yrand] = 'S';
                    }
                }

                //********************************************************************************* 4-affichage du tableau

                do //Boucle pour updater l'affichage du tableau
                {
                    if (sCount == 3) //condition pour sortir de la boucle
                    {
                        sCount++;
                    }

                    Clear();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    //iteration de toutes les lignes du tableau

                    /****************************** CONSTRUCTION DU TABLEAU ******************************/

                    for (i = 0; i < points.GetLength(0); i++) //Boucle des lignes
                    {
                        //Lignes du haut des cases de la premiere ligne 
                        if (i == 0)
                        {
                            for (j = 0; j < points.GetLength(1); j++)
                            {
                                Write($"{ss}{lhorz}");
                            }
                            WriteLine();
                        }

                        //Lignes verticales du premier tiers de chaque case
                        for (j = 0; j < points.GetLength(1); j++)
                        {
                            Write($"{lvert}{espace}");

                            if (j == points.GetLength(1) - 1)  
                            {
                                Write(lvert);
                            }
                        }
                        WriteLine();

                        //Deuxième tiers des cases (Point si vide ou S si bateau)
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        for (j = 0; j < points.GetLength(1); j++)
                        {
                            if (points[i, j] == 'S')
                            {
                                if (mode == Mode.Test) //affichage du bateau en Mode.Test
                                {
                                    Console.BackgroundColor = ConsoleColor.Blue;
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Write($"{lvert}{ss}{ss}{ss}" + points[i, j].ToString().PadRight(4));
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.Cyan;
                                }
                                else //Mode.Play
                                {
                                    Write($"{lvert}{espace}");
                                }

                            }
                            else if (points[i, j] == 's') //update si le user a choisi une case contenant le bateau
                            {
                                Console.BackgroundColor = ConsoleColor.Green;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Write($"{lvert}{ss}{ss}{ss}" + points[i, j].ToString().PadRight(4));
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.Cyan;
                            }
                            else if (points[i, j] == 'X') //update si le user choisi une case sans bateau
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Write($"{lvert}{ss}{ss}{ss}" + points[i, j].ToString().PadRight(4));
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.Cyan;
                            }
                            else
                            {
                                Write($"{lvert}{espace}"); //affichage des cases sans bateau
                            }


                            if (j == points.GetLength(1) - 1)  //derniere ligne des dernieres cases de chaque ligne
                            {
                                Write(lvert);
                            }
                        }
                        WriteLine();

                        //Troisième tiers de chaque case 
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        for (j = 0; j < points.GetLength(1); j++)
                        {
                            Write($"{lvert}{lhorz}");           //base de chaque case

                            if (j == points.GetLength(1) - 1)   //derniere ligne verticale des dernieres cases des lignes
                            {
                                Write(lvert);
                            }
                        }
                        WriteLine();
                    }
                    WriteLine();
                    System.Threading.Thread.Sleep(1000);

                    //********************************************************************************* 4-Demande a l'utilisateur
                    if (sCount < 3)
                    {   
                        Console.ForegroundColor = ConsoleColor.White;
                        TypeLine("Veuillez entrer votre votre coordonnée représentant la LIGNE. Entrez un nombre de 1 à 10: ");
                        do
                        {
                            struseri = Console.ReadLine();
                            isTrue = int.TryParse(struseri, out useri);
                            if (!isTrue)
                            {
                                ForegroundColor = ConsoleColor.Red;
                                TypeLine("Erreur - Vous n'avez pas entré de nombre. Entrez un nombre de 1 à 10 représentant la LIGNE: ");
                                ForegroundColor = ConsoleColor.White;
                            }
                            else if (useri < 1 || useri > 10)
                            {
                                ForegroundColor = ConsoleColor.Red;
                                TypeLine("Erreur - Vous n'avez pas entré un nombre valide. Entrez un nombre de 1 à 10 représentant la LIGNE: ");
                                ForegroundColor = ConsoleColor.White;
                            }
                            else
                            {
                                break;
                            }
                        } while (!isTrue || useri < 1 || useri > 10);

                        System.Threading.Thread.Sleep(1000);
                        TypeLine("Veuillez entrer votre votre coordonnée représentant la COLONNE. Entrez un nombre de 1 à 10: ");
                        do
                        {
                            struserj = Console.ReadLine();
                            isTrue = int.TryParse(struserj, out userj);
                            if (!isTrue)
                            {
                                ForegroundColor = ConsoleColor.Red;
                                TypeLine("Erreur - Vous n'avez pas entré de nombre. Entrez un nombre de 1 à 10 représentant la COLONNE: ");
                                ForegroundColor = ConsoleColor.White;
                            }
                            else if (userj < 1 || userj > 10)
                            {
                                ForegroundColor = ConsoleColor.Red;
                                TypeLine("Erreur - Vous n'avez pas entré un nombre valide. Entrez un nombre de 1 à 10 représentant la COLONNE: ");
                                ForegroundColor = ConsoleColor.White;
                            }
                            else
                            {
                                break;
                            }
                        } while (!isTrue || userj < 1 || userj > 10);

                        //correction du decallage de l'index
                        --useri;
                        --userj;

                        //********************************************************************************* 4-Update de l'affichage du tableau selon les valeurs entrees par l'utilisateur

                        if (points[useri, userj] == 'S')
                        {
                            points[useri, userj] = 's';
                            sCount++;
                        }
                        else if (points[useri, userj] == 's')
                        {
                            continue;
                        }
                        else
                        {
                            points[useri, userj] = 'X';
                        }
                    }
                } while (sCount <= 3);

                //conclusion
                System.Threading.Thread.Sleep(1000);
                Console.ForegroundColor = ConsoleColor.Green;
                TypeLine("Bravo. Vous avez coulé le navire russe... Allez en paix.\n");
                System.Threading.Thread.Sleep(1000);
                Write("Pesez sur ENTER pour quitter. Tapez YES pour rejouer: ");
                doItAgain = Console.ReadLine().ToUpper();

            } while (doItAgain == "YES");
        }
    }
}
